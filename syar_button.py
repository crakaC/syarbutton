# -*- coding: utf-8 -*-
import RPi.GPIO as IO
import tw
import time

def get_syar(str, count):
    for i in range(0, count):
        str += "…"

    return str[0:140]

class SyarButton(tw.TwListener):
    def led(self, r, g, b):
        for pwm, duty in [(self.pwm_r, r), (self.pwm_g, g), (self.pwm_b, b)]:
            pwm.ChangeDutyCycle(duty)

    def reset_state(self):
        self.led(20,1,2)
        self.state = False

    def before(self):
        self.led(0, 0, 10)

    def error(self):
        self.led(100, 0, 0)
        time.sleep(5)
        self.reset_state()

    def success(self):
        self.led(0, 2, 0)
        self.count+=1
        time.sleep(5)
        self.reset_state()

    def switch_callback(self, channel):
        if self.state:
            return
        self.state = True
        str = get_syar(self.text, self.count)
        self.twitter.update(str, self)

    def __init__(self):
        self.twitter = tw.Tw()
        self.text = "( ˘ω˘)ｽﾔｧ"
        self.count = 0
        self.r_pin = 18
        self.g_pin = 17
        self.b_pin = 27
        self.sw_pin = 23
        self.state = False

        IO.setmode(IO.BCM)
        for pin in [self.r_pin, self.g_pin, self.b_pin]:
            IO.setup(pin, IO.OUT)

        IO.setup(self.sw_pin, IO.IN, pull_up_down = IO.PUD_UP)
        
        IO.add_event_detect(self.sw_pin, IO.FALLING, callback = self.switch_callback, bouncetime=100)
        
        self.pwm_r = IO.PWM(self.r_pin, 1000)
        self.pwm_g = IO.PWM(self.g_pin, 1000)
        self.pwm_b = IO.PWM(self.b_pin, 1000)
        
        for pwm, duty in [(self.pwm_r, 20), (self.pwm_g, 1), (self.pwm_b, 2)]:
            pwm.start(duty)
        
        raw_input("hit return to exit\n")
    
        IO.cleanup()

syar = SyarButton()
