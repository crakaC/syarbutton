import RPi.GPIO as IO

class LEDControler:
    def switch_callback(self, channel):
        if self.last_state:
            self.pwm_r.stop()
            self.pwm_g.stop()
            self.pwm_b.stop()
        else :
            self.pwm_r.start(20)
            self.pwm_g.start(1)
            self.pwm_b.start(2)
            
        self.last_state = not self.last_state

    def __init__(self, str):
        self.r_pin = 18
        self.g_pin = 17
        self.b_pin = 27
        self.sw_pin = 23
        self.last_state = False

        IO.setmode(IO.BCM)
        IO.setup(self.r_pin, IO.OUT)
        IO.setup(self.g_pin, IO.OUT)
        IO.setup(self.b_pin, IO.OUT)
        IO.setup(self.sw_pin, IO.IN, pull_up_down = IO.PUD_UP)
        
        IO.add_event_detect(self.sw_pin, IO.BOTH, callback = self.switch_callback, bouncetime=100)
        
        self.pwm_r = IO.PWM(self.r_pin, 1000)
        self.pwm_g = IO.PWM(self.g_pin, 1000)
        self.pwm_b = IO.PWM(self.b_pin, 1000)
        
        raw_input("hit return to exit\n")
    
        IO.cleanup()

LEDControler("test")
