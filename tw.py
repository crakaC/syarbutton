# -*- coding: utf-8 -*-

import twitter
import time

class Tw:
    def __init__(self):
        self.api = twitter.Api(consumer_key='',
                               consumer_secret='',
                               access_token_key='',
                               access_token_secret='')
        

    def update(self, str, listener):
        listener.before()
        try:
            status = self.api.PostUpdate(str)
            print(status.GetText())
        except twitter.TwitterError as e:
            print (e)
            listener.error()
        else:
            listener.success()

class TwListener:
    def before(self): pass
    def error(self): pass
    def success(self): pass
